const submitBtn = document.querySelector('.btn')
const enterPassword = document.querySelector('.enter-password')
const repeatPassword = document.querySelector('.repeat-password')
const span = document.createElement('span')

const error = (password, message) => {
  span.classList.add('error')
  span.innerHTML = message
  password.after(span)
}

submitBtn.addEventListener('click', () => {
  if (!enterPassword.value) {
    error(enterPassword, 'Пароль не було введено')
  } else if (!repeatPassword.value) {
    error(repeatPassword, 'Потрібно ввести однакові значення')
  } else if (enterPassword.value === repeatPassword.value) {
    span.remove()
    alert('Ласкаво просимо!')
  } else {
    span.remove()
    error(repeatPassword, 'Потрібно ввести однакові значення')
  }
})


const eyes = document.querySelectorAll('.fa-eye')
const inputParent = document.querySelector('.password-form')

inputParent.addEventListener('click', (e) => {
  eyes.forEach((eye) => {
    if (e.target === eye) {
      eye.classList.toggle('fa-eye-slash')
      const input = eye.closest('.input-wrapper').querySelector('input')
      input.type = input.type === 'password' ? 'text' : 'password'
    }
  })
})

